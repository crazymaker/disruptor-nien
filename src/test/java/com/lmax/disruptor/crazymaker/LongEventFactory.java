package com.lmax.disruptor.crazymaker;

import com.lmax.disruptor.EventFactory;

class LongEventFactory implements EventFactory {
        @Override
        public Object newInstance() {
            return new LongEvent();
        }
    }
