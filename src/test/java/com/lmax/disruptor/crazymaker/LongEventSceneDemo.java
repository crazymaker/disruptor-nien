package com.lmax.disruptor.crazymaker;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.YieldingWaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.EventHandlerGroup;
import com.lmax.disruptor.dsl.ProducerType;
import com.lmax.disruptor.util.ThreadUtil;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class LongEventSceneDemo {


    @org.junit.Test
    public void testHexagonConsumerDisruptorWithMethodRef() throws InterruptedException {
        // 消费者线程池
        Executor executor = Executors.newCachedThreadPool();
        // 环形队列大小，2的指数
        int bufferSize = 1024;
        // 构造  分裂者 （事件分发者）
        Disruptor<LongEvent> disruptor = new Disruptor<LongEvent>(new LongEventFactory(), bufferSize,
                executor,
                ProducerType.SINGLE,  //多个生产者
                new YieldingWaitStrategy());

        EventHandler consumer1 = new LongEventHandlerWithName("consumer 1");
        EventHandler consumer2 = new LongEventHandlerWithName("consumer 2");
        EventHandler consumer3 = new LongEventHandlerWithName("consumer 3");
        EventHandler consumer4 = new LongEventHandlerWithName("consumer 4");
        EventHandler consumer5 = new LongEventHandlerWithName("consumer 5");
        // 连接 消费者 处理器
        // 可以使用lambda来注册一个EventHandler

        EventHandlerGroup<LongEvent> firstGroup = disruptor.handleEventsWith(consumer1, consumer2);

//        firstGroup.then(consumer3);

        disruptor.after(consumer1).handleEventsWith(consumer3);

//        disruptor.after(consumer2).handleEventsWith(consumer4);

        EventHandlerGroup<LongEvent> eventHandlerGroup = disruptor.after(consumer2);
        eventHandlerGroup.handleEventsWith(consumer4);

        disruptor.after(consumer3, consumer4).handleEventsWith(consumer5);
        // 开启 分裂者（事件分发）
        disruptor.start();
        // 获取环形队列，用于生产 事件
        RingBuffer<LongEvent> ringBuffer = disruptor.getRingBuffer();
        //1生产者，并发生产数据
        final LongEventProducer producer = new LongEventProducer(ringBuffer);
        Thread thread = new Thread() {
            @Override
            public void run() {
                for (long i = 0; true; i++) {
                    producer.onData(i);
                    ThreadUtil.sleepSeconds(1);
                }
            }
        };
        thread.start();
        ThreadUtil.sleepSeconds(5);
    }

    @org.junit.Test
    public void testDemoConsumerDisruptorWithMethodRef() throws InterruptedException {
        // 消费者线程池
        Executor executor = Executors.newCachedThreadPool();
        // 环形队列大小，2的指数
        int bufferSize = 1024;
        // 构造  分裂者 （事件分发者）
        Disruptor<LongEvent> disruptor = new Disruptor<LongEvent>(new LongEventFactory(), bufferSize,
                executor,
                ProducerType.SINGLE,  //多个生产者
                new YieldingWaitStrategy());

        EventHandler consumer1 = new LongEventHandlerWithName("consumer 1");
        EventHandler consumer2 = new LongEventHandlerWithName("consumer 2");
        EventHandler consumer3 = new LongEventHandlerWithName("consumer 3");
        // 连接 消费者 处理器
        // 可以使用lambda来注册一个EventHandler

        EventHandlerGroup<LongEvent> firstGroup = disruptor.handleEventsWith(consumer1, consumer2);

        firstGroup.then(consumer3);


        // 开启 分裂者（事件分发）
        disruptor.start();
        // 获取环形队列，用于生产 事件
        RingBuffer<LongEvent> ringBuffer = disruptor.getRingBuffer();
        //1生产者，并发生产数据
        final LongEventProducer producer = new LongEventProducer(ringBuffer);
        Thread thread = new Thread() {
            @Override
            public void run() {
                for (long i = 0; true; i++) {
                    producer.onData(i);
                    ThreadUtil.sleepSeconds(1);
                }
            }
        };
        thread.start();
        ThreadUtil.sleepSeconds(5);
    }

}
