package com.lmax.disruptor.crazymaker;

class LongEvent {

    //封装业务数据 ，或者  业务对象
        private long value;

        public long getValue() {
            return value;
        }

        public void setValue(long value) {
            this.value = value;
        }
    }